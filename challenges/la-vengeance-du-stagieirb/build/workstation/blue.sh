#!/bin/bash
log_path="/var/log/blue.log"
file_path_crontab="/etc/crontab"
file_path_image="/home/remiFassolle/Pictures/screenshots"
image_name="Screenshot 2023-12-11 112657.png"
pattern_crontab="root[[:space:]]+/home/martine/"
steps=0

flag=$(cat /root/flag4.txt)
rm -rf /root/flag4.txt
touch $log_path
chmod 600 $log_path

while true; do
    steps=0
    if [ -e $file_path_crontab ]; then
        echo "File exists at $(date)" > $log_path

        if grep -Eq "$pattern_crontab" "$file_path_crontab"; then
            echo "Pattern found at $(date)" >> $log_path
            steps=$((steps + 1))
        else
            echo "Pattern not found at $(date)" >> $log_path
        fi

    else
        echo "File does not exist at $(date)" >> $log_path
    fi

    if [ -e "$file_path_image/$image_name" ]; then
        echo "File exists at $(date)" >> $log_path

        warnings=$(exiftool -warning -q -r $file_path_image/*.png)

        # Check if there are any warnings
        if [ -n "$warnings" ]; then
            echo "Warnings detected:" >> $log_path
            echo "$warnings" >> $log_path
            steps=$((steps + 1))
        else
            echo "No warnings found."  >> $log_path
        fi

    else
        echo "File does not exist at $(date)" >> $log_path
    fi
    echo "" >> $log_path
    echo "======================================" >> $log_path
    echo "" >> $log_path
    if [ "$steps" -gt 0 ]; then   
        echo "Still found $steps problems. Please fix them." >> $log_path
    else
        echo "All problems fixed. Congratulations!" >> $log_path
        echo "Flag: $flag" >> $log_path
    fi

    sleep 10
done

