cd ~/Desktop/Kernel-Module-Project
vim file_operations.c
gcc -o kernel_module file_operations.c
./kernel_module  # Testing the initial implementation
cd ~/Documents/Project-Notes
nano observations_and_ideas.md
git add observations_and_ideas.md
git commit -m "Record observations and ideas for project improvement"
git push origin main
cd ~/Downloads
wget https://www.kernel.org/pub/linux/kernel/v5.x/linux-5.10.tar.gz
tar -zxvf linux-5.10.tar.gz
cd linux-5.10
make menuconfig  # Configuring the kernel for testing
make
sudo make modules_install
sudo make install
cd ~/Documents/Meetings
nano team_meeting_2023-03-15.txt
git add team_meeting_2023-03-15.txt
git commit -m "Capture discussion points from the team meeting"
git push origin main
