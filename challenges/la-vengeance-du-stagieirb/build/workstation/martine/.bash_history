cd ~/Documents/Project-Management
nano project_timeline_2023.xlsx
git add project_timeline_2023.xlsx
git commit -m "Update project timeline for Q2"
git push origin main
cd ~/Documents/Meetings
nano managerial_meeting_agenda_2023-03-20.doc
libreoffice managerial_meeting_agenda_2023-03-20.doc  # Preparing for the meeting
git add managerial_meeting_agenda_2023-03-20.doc
git commit -m "Prepare agenda for March 20 meeting"
git push origin main
cd ~/Documents/Emails
thunderbird  # Composing emails to stakeholders
git add emails_to_stakeholders.txt
git commit -m "Send project updates to stakeholders"
git push origin main
