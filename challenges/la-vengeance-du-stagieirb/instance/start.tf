terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
    }
  }
}

provider "docker" {
  host = "ssh://${var.dockeruser}@${var.dockerhost}:${var.dockerport}"
}

# region la-vengeance-du-stagieirb

resource "docker_container" "docker_image_la-vengeance-du-stagieirb-workstation" {
  image = "${var.docker_registry}/la-vengeance-du-stagieirb-workstation:${var.build_version}"
  name  = "docker_${var.instance_id}_la-vengeance-du-stagieirb-workstation"
  upload {
    file    = "/home/remiFassolle/flag1.txt"
    content = random_string.flag1.result
  }
  upload {
    file    = "/home/martine/flag2.txt"
    content = random_string.flag2.result
  }
  upload {
    file    = "/root/flag3.txt"
    content = random_string.flag3.result
  }
  upload {
    file    = "/root/flag4.txt"
    content = random_string.flag4.result
  }
  ports {
    internal = 22
  }
}

# endregion

# region la-vengeance-du-stagieirb-apache

resource "docker_container" "docker_image_la-vengeance-du-stagieirb-apache" {
  image = "${var.docker_registry}/la-vengeance-du-stagieirb-apache:${var.build_version}"
  name  = "docker_${var.instance_id}_la-vengeance-du-stagieirb-apache"
  ports {
    internal = 80
  }
}


#
# Génération des flag
#
resource "random_string" "flag1" {
  keepers = {instance_id = var.instance_id}
  length = 32
  special = false
  min_upper = 4
  min_lower = 4
  min_numeric = 4
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/flags; echo ${random_string.flag1.result} > ${path.module}/flags/flag1.txt"
  }
}
resource "random_string" "flag2" {
  keepers = {instance_id = var.instance_id}
  length = 32
  special = false
  min_upper = 4
  min_lower = 4
  min_numeric = 4
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/flags; echo ${random_string.flag2.result} > ${path.module}/flags/flag2.txt"
  }
}
resource "random_string" "flag3" {
  keepers = {instance_id = var.instance_id}
  length = 32
  special = false
  min_upper = 4
  min_lower = 4
  min_numeric = 4
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/flags; echo ${random_string.flag3.result} > ${path.module}/flags/flag3.txt"
  }
}
resource "random_string" "flag4" {
  keepers = {instance_id = var.instance_id}
  length = 32
  special = false
  min_upper = 4
  min_lower = 4
  min_numeric = 4
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/flags; echo ${random_string.flag4.result} > ${path.module}/flags/flag4.txt"
  }
}

# endregion

#region Variables

variable "instance_id" {
  type = number
}

variable "dockerhost" {
  type = string
}

variable "dockerport" {
  type = string
}

variable "dockeruser" {
  type = string
}

variable "docker_registry" {
  type = string
}

variable "build_version" {
  type = string
}

output "port22_workstation" {
  value = docker_container.docker_image_la-vengeance-du-stagieirb-workstation.ports[0].external
}
output "port80_apache" {
  value = docker_container.docker_image_la-vengeance-du-stagieirb-apache.ports[0].external
}

# endregion
