#!/bin/bash

LOG_FILE="/var/log/health-check.log"

if [ ! -e "$LOG_FILE" ]; then
    echo "Challenge has been launched for 1 min." > "$LOG_FILE"
else
    last_line=$(tail -n 1 "$LOG_FILE")
    last_value=$(echo "$last_line" | grep -oE '[0-9]+')
    new_value=$((last_value + 1))
    echo "Challenge has been launched for $new_value min." >> "$LOG_FILE"
fi