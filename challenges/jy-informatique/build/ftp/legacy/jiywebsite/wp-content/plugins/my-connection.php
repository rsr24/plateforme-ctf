<?php
function my_ssh_connect_and_execute() {
    $ssh_host = 'localhost';
    $ssh_port = 22;
    $ssh_private_key = <<<EOD
-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABlwAAAAdzc2gtcn
NhAAAAAwEAAQAAAYEAv75CZHvcACnVtCqGY0ZGIfs451cvMMdbIHmvq7LtaSkFsuozIpCr
RAdV5YpBp0YPrcP/LsjoZ1Mp2w/CZFysjrimHIJBjNDk3ClE0QKmnSm1wn0dEjcWyXrXBj
TFPx9xuVSjal8FUrWuwaXZOVaOWtOYqYM05GEG4k4ykSYTA6j5UH7zMdNDL5L0n43caG0E
iZgONMdt0ca8iqWTKrD4/rQ/GSsPXGtPj7CrIqjyLGmt2jwRD7n8ANwS/DKkNSwXrtfPF1
2C0C4L+vHObiyfRAepe+jlNg2NtcF9XmVlrf5rbRpeT2mOn/R3cIDQKQ33uvy5i6VikJXg
SnUzy+jsPSg62SZ10tupYzSIXJn0v49lLjl6EoxVeprHOpqhQZcMBjhja9oisoeptLjxoH
DrdAaheAUnbuf32XEMYOzLWxCNAnRHOMCq9o0PFr1bQjE4BHKp1AbWS3hEBp3NuxBji3Ec
3lWDU3NzWdwqXzVibbmeZsf2YwNnX0fkvLH7FPTVAAAFiKcGi2qnBotqAAAAB3NzaC1yc2
EAAAGBAL++QmR73AAp1bQqhmNGRiH7OOdXLzDHWyB5r6uy7WkpBbLqMyKQq0QHVeWKQadG
D63D/y7I6GdTKdsPwmRcrI64phyCQYzQ5NwpRNECpp0ptcJ9HRI3Fsl61wY0xT8fcblUo2
pfBVK1rsGl2TlWjlrTmKmDNORhBuJOMpEmEwOo+VB+8zHTQy+S9J+N3GhtBImYDjTHbdHG
vIqlkyqw+P60PxkrD1xrT4+wqyKo8ixprdo8EQ+5/ADcEvwypDUsF67XzxddgtAuC/rxzm
4sn0QHqXvo5TYNjbXBfV5lZa3+a20aXk9pjp/0d3CA0CkN97r8uYulYpCV4Ep1M8vo7D0o
OtkmddLbqWM0iFyZ9L+PZS45ehKMVXqaxzqaoUGXDAY4Y2vaIrKHqbS48aBw63QGoXgFJ2
7n99lxDGDsy1sQjQJ0RzjAqvaNDxa9W0IxOARyqdQG1kt4RAadzbsQY4txHN5Vg1Nzc1nc
Kl81Ym25nmbH9mMDZ19H5Lyx+xT01QAAAAMBAAEAAAGACGvEcjHPYHF16X75nngKYfksz1
owoCGNQsCdYrrNuvHahnhLLSAx6V+yGc3n4r8Mtrq2JRmdPVZ1buHDDZHVYUJCR1vAIb1V
48ZrN4u4ggwAJ83DXlatiUU8FtFv/Tz2C2focmZAb1i711TflKlUwK/s0zsIMmSBfyRki6
Pir2U3J94fc6d92qPHWw+Jp8tr6NgU/aPpko46HxO1Aim6EcOcRtBxkQNEgADID1Jgqmss
v7omHKz0IJyJt+pIdG5ee2a+3zZDO/kCH3fhJ+PxFCw5LzuGTGTIt56mEILK8fugHTRwZ9
mjbIvQxblodna9acDDRHV2Sf6WaWcqcT0++1uElOUAkeW18RnrVBNCJyHceVM/tP0eGxuH
xrnwFnr/birvDu/5BC95nATBaTc8nQTwEUOKmmUlFVbTfVK8BdPLgJb3772xWM9xEL93Wp
0lwODFIW+epU760/NOpkqj5uFxwuzJMl8BJ/ZzlQP+7NanFUKEiW9bOKjVv0k5ghvZAAAA
wQDDsdMJxf7UukZzqTlVWCVkPo9oC4kixTrwwS/9pnHk0C49pNyUa6ofjX4LCJYWiGhKWn
M/u/InnPkiJuHKrycAQ8WEgJK4Q/R7up41scADeK/897bz/pp/aI1gTZJfXW8qpnfcUclW
03MeBmoYvdtDuiQ6wK5w2X5HCsI9fnjvxoFjXqQ7BsEoII0Dsyxb980y/x6Ow4mvi5f7Mn
Kqrgt0/Cvg/gV/4Deasalx8PxwkPSOCG3XrVN+3nceVlxow40AAADBAOxRf8pltxRmu40w
n0I+lG/i9p88acPZxoxU9xWRlLcu+n2gR7JzK9PqGq219PTvTcjaHCSCRI3IY4/CY9/o95
rvyBUqIKCK76aCcV9QVLqhnNP8CM3QBiEJkCQz3qn8VBRXnxp+BFCwu2F9kRNbhDLUk84x
gawew1yZ2Eh32/ammL/uyVLO5xqn+8QXNX8EWkbbcU0COMmN36WWhsoIeHtlzoA2GE/MRL
HY7gZNg9YrClu3L8V0XJReW8z+RHC0rQAAAMEAz7ZhoG3OP1ka+eJN8a+QCGSHJoWzSYxJ
3zw2T4LFWtIDChDs+/56QvVG2uyjUHl8S+hPmkJKuxp5xdTp0A4T7rBwD4NUP9T5yvBbKi
bOTTGXmF57XVgKZimseCcjUy1qZOSut1SEpwm2oAvSOQT2ztrWTPud8IZpKmBYtFqTxoi+
WTRZ7KXpQlqJIhVW37+AGoizOmTHDNsD3CK6ttYjLw3whbK4DA//w5j9SigRv7rg/JTDVC
sIvtsAQ8cfDZ3JAAAAD21hdGhpZXVAbm90YXJjaAECAw==
-----END OPENSSH PRIVATE KEY-----
EOD;

    $connection = ssh2_connect($ssh_host, $ssh_port);
    if (!$connection) {
        return 'Connection failed';
    }

    if (!ssh2_auth_pubkey_file($connection, $ssh_user, 'path/to/public/key.pub', $ssh_private_key)) {
        return 'Authentication failed';
    }

    $stream = ssh2_exec($connection, 'some-command');
    stream_set_blocking($stream, true);
    $stream_out = ssh2_fetch_stream($stream, SSH2_STREAM_STDIO);
    $output = stream_get_contents($stream_out);

    return $output;
}

