<?php
/**
 * Plugin Name: Remote SSH Files Plugin
 * Description: Fetches content from a remote server via SSH.
 * Version: 1.0
 * Author: CrucialDev
 */

// Perform the SSH connection and fetch content
function fetch_remote_content() {
    include 'my-connection.php';
}

// Add a shortcode to display content
function display_remote_content() {
    fetch_remote_content();
}
add_shortcode('show_remote_content', 'display_remote_content');
