-- Création de la table City
CREATE TABLE IF NOT EXISTS City (
                                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                                    name TEXT NOT NULL,
                                    department_number INTEGER NOT NULL,
                                    region TEXT NOT NULL,
                                    country TEXT NOT NULL,
                                    distance INTEGER NOT NULL


);

-- Insertion de données dans la table City
INSERT INTO City (name, department_number, region, country, distance) VALUES ('Pessac', 33, 'Aquitaine', 'France', 0);
INSERT INTO City (name, department_number, region, country, distance) VALUES ('Bordeaux', 33, 'Aquitaine', 'France', 15);
INSERT INTO City (name, department_number, region, country, distance) VALUES ('Talence', 33, 'Aquitaine', 'France', 10);
INSERT INTO City (name, department_number, region, country, distance) VALUES ('Mérignac', 33, 'Aquitaine', 'France', 5);
INSERT INTO City (name, department_number, region, country, distance) VALUES ('Gradignan', 33, 'Aquitaine', 'France', 15);

-- Création de la table User
CREATE TABLE IF NOT EXISTS User (
                                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                                    firstname TEXT NOT NULL,
                                    lastname TEXT NOT NULL,
                                    email TEXT NOT NULL,
                                    password TEXT NOT NULL,
                                    city_id INTEGER NOT NULL,
                                    FOREIGN KEY(city_id) REFERENCES City(id)
);

-- Insertion de données dans la table User
INSERT INTO User (firstname, lastname, email, password, city_id) VALUES ('John', 'Doe', 'john.doe@gmail.com', 'bdlp-voiture-nmaa', 1);
INSERT INTO User (firstname, lastname, email, password, city_id) VALUES ('Jane', 'Doe', 'jane.doe@gmail.com', 'bdlp-camion-nmaa', 1);
INSERT INTO User (firstname, lastname, email, password, city_id) VALUES ('John', 'Smith', 'john.smith@gmail.com', 'bdlp-moto-nmaa', 2);
INSERT INTO User (firstname, lastname, email, password, city_id) VALUES ('Tom', 'Smith', 'tom.smith@gmail.com', 'bdlp-motdepasse-nmaa',3);
INSERT INTO User (firstname, lastname, email, password, city_id) VALUES ('Léa', 'Martin', 'lea.martin@gmail.com', 'bdlp-avion-nmaa', 4);
INSERT INTO User (firstname, lastname, email, password, city_id) VALUES ('Emma', 'Martin', 'emma.martin@gmail.com', 'bdlp-bateau-nmaa', 4);
INSERT INTO User (firstname, lastname, email, password, city_id) VALUES ('Jean-Yves', 'Dupont', 'jean-yves@gmail.com', 'bdlp-quad-nmaa',5);

-- Création de la table Comment
CREATE TABLE IF NOT EXISTS Comment (
                                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                                    content TEXT NOT NULL,
                                    user_id INTEGER NOT NULL,
                                    FOREIGN KEY(user_id) REFERENCES User(id)
);

-- Insertion de données dans la table Comment
INSERT INTO Comment (content, user_id) VALUES ('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae nulla euismod, aliquam nisl quis, aliquam nisl. Nulla facilisi. Sed euismod, nisl quis aliquam lacinia, nisl nisl aliquam nisl, quis aliquam nisl nisl quis.', 1);
INSERT INTO Comment (content, user_id) VALUES ('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae nulla euismod, aliquam nisl quis, aliquam nisl. Nulla facilisi. Sed euismod, nisl quis aliquam lacinia, nisl nisl aliquam nisl, quis aliquam nisl nisl quis.', 2);
INSERT INTO Comment (content, user_id) VALUES ('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae nulla euismod, aliquam nisl quis, aliquam nisl. Nulla facilisi. Sed euismod, nisl quis aliquam lacinia, nisl nisl aliquam nisl, quis aliquam nisl nisl quis.', 3);
INSERT INTO Comment (content, user_id) VALUES ('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae nulla euismod, aliquam nisl quis, aliquam nisl. Nulla facilisi. Sed euismod, nisl quis aliquam lacinia, nisl nisl aliquam nisl, quis aliquam nisl nisl quis.', 4);
INSERT INTO Comment (content, user_id) VALUES ('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae nulla euismod, aliquam nisl quis, aliquam nisl. Nulla facilisi. Sed euismod, nisl quis aliquam lacinia, nisl nisl aliquam nisl, quis aliquam nisl nisl quis.', 5);
INSERT INTO Comment (content, user_id) VALUES ('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae nulla euismod, aliquam nisl quis, aliquam nisl. Nulla facilisi. Sed euismod, nisl quis aliquam lacinia, nisl nisl aliquam nisl, quis aliquam nisl nisl quis.', 6);
INSERT INTO Comment (content, user_id) VALUES ('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae nulla euismod, aliquam nisl quis, aliquam nisl. Nulla facilisi. Sed euismod, nisl quis aliquam lacinia, nisl nisl aliquam nisl, quis aliquam nisl nisl quis.', 7);
