const path = require("path");
const sqlite3 = require("sqlite3").verbose();
// Remplacez 'nom_de_votre_base_de_donnees.db' par le nom de votre base de données SQLite

const pathDb = path.join(__dirname, "database.sqlite");
console.log(pathDb);
const db = new sqlite3.Database(pathDb);

// Création de la table City
db.run(`CREATE TABLE IF NOT EXISTS City (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL,
    department_number INTEGER NOT NULL,
    region TEXT NOT NULL,
    country TEXT NOT NULL,
    distance INTEGER NOT NULL
);`);

// Insertion de données dans la table City
db.run(
  `INSERT INTO City (name, department_number, region, country, distance) VALUES ('Pessac', 33, 'Aquitaine', 'France', 0);`,
);
db.run(
  `INSERT INTO City (name, department_number, region, country, distance) VALUES ('Bordeaux', 33, 'Aquitaine', 'France', 15);`,
);
db.run(
  `INSERT INTO City (name, department_number, region, country, distance) VALUES ('Talence', 33, 'Aquitaine', 'France', 10);`,
);
db.run(
  `INSERT INTO City (name, department_number, region, country, distance) VALUES ('Mérignac', 33, 'Aquitaine', 'France', 5);`,
);
db.run(
  `INSERT INTO City (name, department_number, region, country, distance) VALUES ('Gradignan', 33, 'Aquitaine', 'France', 15);`,
);

// Création de la table User
db.run(`CREATE TABLE IF NOT EXISTS User (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    firstname TEXT NOT NULL,
    lastname TEXT NOT NULL,
    email TEXT NOT NULL,
    password TEXT NOT NULL,
    city_id INTEGER NOT NULL,
    FOREIGN KEY(city_id) REFERENCES City(id)
);`);

// Insertion de données dans la table User
db.run(
  `INSERT INTO User (firstname, lastname, email, password, city_id) VALUES ('John', 'Doe', 'john.doe@gmail.com', ${btoa(
    "motdepassefacile-nmaa",
  )}, 1);`,
);
db.run(
  `INSERT INTO User (firstname, lastname, email, password, city_id) VALUES ('Jane', 'Doe', 'jane.doe@gmail.com', ${btoa(
    "motdepassemoyenbof-nmaa",
  )}, 1);`,
);
db.run(
  `INSERT INTO User (firstname, lastname, email, password, city_id) VALUES ('John', 'Smith', 'john.smith@gmail.com', ${btoa(
    "motdepassedifficile-nmaa",
  )}, 2);`,
);
db.run(
  `INSERT INTO User (firstname, lastname, email, password, city_id) VALUES ('Tom', 'Smith', 'tom.smith@gmail.com', ${btoa(
    "motdepassequipassepas-nmaa",
  )}, 3);`,
);
db.run(
  `INSERT INTO User (firstname, lastname, email, password, city_id) VALUES ('Léa', 'Martin', 'lea.martin@gmail.com', ${btoa(
    "motdepassequipasse-nmaa",
  )}, 4);`,
);
db.run(
  `INSERT INTO User (firstname, lastname, email, password, city_id) VALUES ('Emma', 'Martin', 'emma.martin@gmail.com', ${btoa(
    "motdepasseetonnant-nmaa",
  )}, 4);`,
);
db.run(
  `INSERT INTO User (firstname, lastname, email, password, city_id) VALUES ('Jean-Yves', 'Dupont', 'jean-yves@gmail.com', ${btoa(
    "motdepassepasdur-nmaa",
  )}, 5);`,
);

// Création de la table Comment
db.run(`CREATE TABLE IF NOT EXISTS Comment (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    content TEXT NOT NULL,
    user_id INTEGER NOT NULL,
    FOREIGN KEY(user_id) REFERENCES User(id)
);`);

// Insertion de données dans la table Comment
db.run(
  `INSERT INTO Comment (content, user_id) VALUES ('Lorem ipsum dolor sit amet, consectetur adipiscing elit...', 1);`,
);
db.run(
  `INSERT INTO Comment (content, user_id) VALUES ('Lorem ipsum dolor sit amet, consectetur adipiscing elit...', 2);`,
);
db.run(
  `INSERT INTO Comment (content, user_id) VALUES ('Lorem ipsum dolor sit amet, consectetur adipiscing elit...', 3);`,
);
db.run(
  `INSERT INTO Comment (content, user_id) VALUES ('Lorem ipsum dolor sit amet, consectetur adipiscing elit...', 4);`,
);
db.run(
  `INSERT INTO Comment (content, user_id) VALUES ('Lorem ipsum dolor sit amet, consectetur adipiscing elit...', 5);`,
);
db.run(
  `INSERT INTO Comment (content, user_id) VALUES ('Lorem ipsum dolor sit amet, consectetur adipiscing elit...', 6);`,
);
db.run(
  `INSERT INTO Comment (content, user_id) VALUES ('Lorem ipsum dolor sit amet, consectetur adipiscing elit...', 7);`,
);

// Fermer la connexion à la base de données
db.close();
