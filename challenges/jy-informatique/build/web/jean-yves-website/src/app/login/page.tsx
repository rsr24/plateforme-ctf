import React from "react";
import Link from "next/link";

const Page = () => {
  return (
    <main className="min-h-screen flex flex-col justify-center items-center gap-16">
      Le site est en cours de construction et cette page n&apos;est pas encore
      disponible.
      <Link href={"/"}>
        <button className="p-2 border border-gray-400 rounded-md hover:cursor-pointer hover:bg-gray-400 hover:text-white hover:border-transparent transition duration-100 ease-in-out hover:shadow-lg">
          Retourner à l&apos;accueil
        </button>
      </Link>
    </main>
  );
};

export default Page;
