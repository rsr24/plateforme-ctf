import sqlite3 from "sqlite3";
import path from "path";
import * as util from "util";
import { City } from "@/types";

async function get_city_by_id(id: unknown) {
  try {
    const dbPath = path.resolve(
      __dirname,
      "../../../../../../src/data/database.sqlite",
    );
    const db = new sqlite3.Database(dbPath);
    const query = util.promisify(db.all).bind(db);

    const sql = "SELECT * FROM City WHERE id = " + id;
    const city = await query(sql);
    db.close();
    return city;
  } catch (e) {
    console.log("error : ", e);
  }
}

async function get_city_by_name(name: unknown) {
  try {
    const dbPath = path.resolve(
      __dirname,
      "../../../../../../src/data/database.sqlite",
    );
    const db = new sqlite3.Database(dbPath);
    const query = util.promisify(db.all).bind(db);

    const sql = "SELECT * FROM City WHERE name = " + name;
    const city = await query(sql);
    db.close();
    return city;
  } catch (e) {
    console.log("error : ", e);
  }
}

export async function GET(req: Request, res: Response) {
  const cityId = decodeURIComponent(req.url.split("/")[5]);
  try {
    const city = await get_city_by_name(cityId);
    return new Response(JSON.stringify(city), { status: 200 });
  } catch (e) {
    return new Response(`City not found !`, { status: 404 });
  }
}
