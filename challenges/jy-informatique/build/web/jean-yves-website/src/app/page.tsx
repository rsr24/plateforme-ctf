import sqlite3 from "sqlite3";
import path from "path";
import * as util from "util";
import SearchBar from "@/app/components/SearchBar";
import { City, User, Comment } from "@/types";
import Link from "next/link";
import React from "react";
import Scene from "./components/Scene";

export default async function Home() {
  const dbPath = path.resolve(__dirname, "../../../src/data/database.sqlite");
  const db = new sqlite3.Database(dbPath);
  const query = util.promisify(db.all).bind(db);

  async function fetchData() {
    try {
      const cities = (await query("SELECT * FROM City")) as City[];
      const comments = (await query("SELECT * FROM Comment")) as Comment[];
      const users = (await query("SELECT * FROM User")) as User[];
      return { cities, comments, users };
    } catch (err) {
      console.log("error while fetching cities", err);
      throw err;
    } finally {
      db.close();
    }
  }

  const { cities, comments, users } = await fetchData();

  return (
    <div className="relative flex flex-row">
      <main className="min-h-screen p-16 flex flex-col gap-16 max-w-6xl">
        <section className="">
          <h2 className="text-3xl">Qui suis-je ?</h2>
          <p>
            Je répars vos ordinateurs et règle vos soucis informatiques. Je suis
            un passionné avec beaucoup d&apos;expériences dans le domaine.
            N&apos;hésitez pas à me contacter pour plus d&apos;informations.
          </p>
        </section>
        <section className="">
          <h2 className="text-3xl">Comment me contacter ?</h2>
          <p>
            Vous pouvez me contacter par mail à l&apos;adresse suivante :{" "}
            <a href="mailto:jeanyves@gmail.com">jeanyves@gmail.com</a> ou par
            téléphone au <a href="tel:+33611223344">06 11 22 33 44</a>.
          </p>
        </section>
        <section className="">
          <h2 className="text-3xl">Quels sont mes tarifs ?</h2>
          <p>
            Mes tarifs sont de 20€ de l&apos;heure pour les prestations
            classiques. Je peux également vous faire un devis si vous le
            souhaitez pour des prestations sur mesure !
          </p>
        </section>
        <section>
          <h2 className="text-3xl">Où suis-je situé ?</h2>
          <p>
            Je suis situé à Pessac près de la ligne B du tram. Tapez le nom de
            votre ville dans la barre de recherche ci-dessous pour voir si je me
            déplace jusqu&apos;à chez vous.
          </p>
          <SearchBar cities={cities} />
        </section>

        <section className="overflow-scroll">
          <h2 className="text-3xl">Avis de mes clients</h2>
          <div className="mt-16 w-full max-w-screen-lg flex justify-center">
            <div className="flex flex-row gap-8 flex-wrap justify-center">
              {comments.map((comment) => {
                const user = users.find((user) => user.id === comment.user_id)!;
                return (
                  <div
                    key={comment.id}
                    className="p-8 border h-64 rounded-lg aspect-square bg-gray-200 flex flex-col justify-center "
                  >
                    <h4 className="text-xl">{user.firstname}</h4>
                    <p>{comment.content}</p>
                  </div>
                );
              })}
            </div>
          </div>
        </section>
      </main>
      <Scene />
    </div>
  );
}
