"use client";

import { useState, ChangeEvent, FormEvent } from "react";

type City = {
  id: number;
  name: string;
};
const SearchBar = ({ cities }: { cities: City[] }) => {
  const [searchValue, setSearchValue] = useState("");
  const [searchResult, setSearchResult] = useState("");
  const handleSearchChange = async (event: ChangeEvent<HTMLInputElement>) => {
    setSearchValue(event.target.value);
  };
  const handleSubmit = async (e: FormEvent) => {
    e.preventDefault();
    const url = `/api/cities/"${searchValue}"`;
    console.log(url);

    let city = await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    }).then((res) => res.json());
    if (city.length === 0) {
      city = "Je ne me déplace pas dans votre ville.";
    } else if (city.length === 1) {
      city = `Je me déplace à ${city[0].name}.`;
    } else {
      city = JSON.stringify(city);
    }
    setSearchResult(city);
  };

  return (
    <>
      <form onSubmit={handleSubmit} className="w-full flex justify-center p-8">
        <input
          type="text"
          className="w-1/2 p-2 border border-gray-400 rounded"
          placeholder="Votre ville"
          value={searchValue}
          onChange={handleSearchChange}
        />
        <input
          type="submit"
          value="Rechercher"
          className="ml-4 p-2 border border-gray-400 rounded-md hover:cursor-pointer hover:bg-gray-400 hover:text-white hover:border-transparent transition duration-100 ease-in-out hover:shadow-lg"
        />
      </form>
      <p>{searchResult}</p>
    </>
  );
};

export default SearchBar;
