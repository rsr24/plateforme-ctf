export type City = {
  id: number;
  name: string;
};
export type Comment = {
  id: number;
  content: string;
  user_id: number;
};
export type User = {
  id: number;
  firstname: string;
  lastname: string;
  email: string;
  city_id: number;
};
