terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
    }
  }
}

provider "docker" {
  host = "ssh://${var.dockeruser}@${var.dockerhost}:${var.dockerport}"
}

# region jy-ssh-tunnel

resource "docker_container" "docker_image_jy-ssh-tunnel" {
  # using rtmp interception kali image
  image = "${var.docker_registry}/rtmp-interception-kali:${var.build_version}"
  name  = "docker_${var.instance_id}_jy-ssh-tunnel"
  upload {
    file = "/home/kali/linpeas.sh"
    source = "linpeas.sh"
  }
  ports {
    internal = 22
  }
  networks_advanced {
    name = docker_network.docker_private_network_jy-info.name
  }
}

# end region

# region jy-web

resource "docker_container" "docker_image_jy-server" {
  image = "${var.docker_registry}/jy-server:${var.build_version}"
  name  = "docker_${var.instance_id}_jy-server"
  ports {
    internal = 80
  }
  upload {
    file    = "/home/jean-yves/web/ftp.txt"
    content = random_string.ftp_flag.result
  }
  upload {
    file    = "/home/jean-yves/jean.txt"
    content = random_string.user_flag.result
  }
  upload {
    file    = "/root/root.txt"
    content = random_string.root_flag.result
  }
  networks_advanced {
    name = docker_network.docker_private_network_jy-info.name
  }
}

# Génération des flags user et root
resource "random_string" "ftp_flag" {
  keepers = {instance_id = var.instance_id}
  length = 32
  special = false
  min_upper = 4
  min_lower = 4
  min_numeric = 4
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/flags; echo ${random_string.ftp_flag.result} > ${path.module}/flags/ftp_flag.txt"
  }
}

resource "random_string" "user_flag" {
  keepers = {instance_id = var.instance_id}
  length = 32
  special = false
  min_upper = 4
  min_lower = 4
  min_numeric = 4
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/flags; echo ${random_string.user_flag.result} > ${path.module}/flags/user_flag.txt"
  }
}

resource "random_string" "root_flag" {
  keepers = {instance_id = var.instance_id}
  length = 32
  special = false
  min_upper = 4
  min_lower = 4
  min_numeric = 4
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/flags; echo ${random_string.root_flag.result} > ${path.module}/flags/root_flag.txt"
  }
}

# endregion

# region Network

resource "docker_network" "docker_private_network_jy-info" {
  name = "docker_${var.instance_id}_jy-info_subnet"
}

# endregion


# region Variables

variable "instance_id" {
  type = number
}

variable "dockerhost" {
  type = string
}

variable "dockerport" {
  type = string
}

variable "dockeruser" {
  type = string
}

variable "docker_registry" {
  type = string
}

variable "build_version" {
  type = string
}

output "port" {
  value = docker_container.docker_image_jy-ssh-tunnel.ports[0].external
}

# endregion
