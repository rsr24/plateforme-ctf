class Profile::InfoController < ApplicationController
  before_action :authenticate_user!
  def show
    user = User.find(params[:id])
    render json: {email: user.email, bcrypt_password: user.encrypted_password, hint: user.hint}
  end
end
