require "test_helper"

class Profile::InfoControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get profile_info_show_url
    assert_response :success
  end
end
