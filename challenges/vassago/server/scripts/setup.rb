# frozen_string_literal: true

User.create(email: "admin@example.com", password: ENV.fetch('ADMIN_PASSWORD',"password123"), hint: "lemme know if you need help. - John", token: ENV.fetch('ADMIN_TOKEN',"default_admin_token")).save!
User.create(email: "email@example.com", password: "enseirb123", hint: "you already know it", token: ENV.fetch('USER_TOKEN',"default_user_token")).save!
