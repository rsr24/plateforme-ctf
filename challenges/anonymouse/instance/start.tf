terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
    }
  }
}

provider "docker" {
  host = "ssh://${var.dockeruser}@${var.dockerhost}:${var.dockerport}"
}

# Create a container
resource "docker_container" "docker_image" {
  image = "${var.docker_registry}/anonymouse:${var.build_version}"
  name  = "docker_${var.instance_id}_anonymouse"
  ports {
    internal = 22
  }
  upload {
      file = "/home/level1/flag.txt"
      content = "FLAGY9$dlj?D*j1=b?e*IqKr"
    }
}

#
# Génération du flag
#
resource "random_string" "flag" {
  keepers = {instance_id = var.instance_id}
  length = 16
  special = false
  min_upper = 4
  min_lower = 4
  min_numeric = 4
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/flags; echo ${random_string.flag.result} > ${path.module}/flags/flag.txt"
  }
}

variable "instance_id" {
  type = number
}

variable "dockerhost" {
  type = string
}

variable "dockerport" {
  type = string
}

variable "dockeruser" {
  type = string
}

variable "build_version" {
  type = string
}

variable "docker_registry" {
  type = string
}

output "port" {
  value = docker_container.docker_image.ports[0].external
}

