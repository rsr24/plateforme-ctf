<?php
$data = $_POST;
$login = $_POST['username'];
$pass = $_POST['password'];

if (isset($login) && isset($pass)) {
    try {
        $file = '/var/www/html/db/database.sqlite';
        $db = new PDO('sqlite:'.$file);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        
        if ($login == "admin" && $pass == "admin") {
            $stmt = $db->prepare('SELECT * FROM users WHERE login = :login AND password = :password');
            $stmt->bindParam(':login', $login);
            $stmt->bindParam(':password', $pass);
            $stmt->execute();
            $result = $stmt->fetchAll();
            print_r("Bien essayé mais ce n'est pas aussi facile !!");
            print_r("\nVoici un indice pour trouver le vrai flag : ".$result[0]['description']);
        } else if ($login == "squanchy" && $pass == "ç4v4squ4nch3r") {
            
        } else {
            $regexprep = $db->prepare('SELECT * FROM users WHERE login LIKE :login');
            $regexprep->bindParam(':login', $login);
            $regexprep->execute();
            $regexresult = $regexprep->fetchAll();
            foreach ($regexresult as $row) {
                print_r("\nLogin : ".$row['login']);
                print_r("\nPassword : ".$row['password']);
                print_r("\nDescription : ".$row['description']);
            }
        }
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}