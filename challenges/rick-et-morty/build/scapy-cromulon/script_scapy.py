from scapy.all import send, ICMP, IP, UDP, sniff
import threading
import time
import socket
import psutil

HOST_IP = socket.gethostbyname(socket.gethostname())
HOST_PORT = 12137 # Pour C137 la planète dont Rick vient

TARGET_IP =  socket.gethostbyname('rick-et-morty_scapy-rick_1.rick-et-morty_network')
TARGET_PORT = 12137

IFACE="eth0"

# A global variable used to stop the first part of the challenge and launch the second part
first_step_active = True

# A global variable to see if the second part end and start to send the flag
second_step_active = False

# Function sending ICMP request
def send_first_round():
    global first_step_active
    icmp_pkt = IP(src=HOST_IP, dst=TARGET_IP, )/ICMP(type = 8)/"Show me what you got"
    while first_step_active :
        send(icmp_pkt, iface=IFACE, verbose=False)
        time.sleep(7)
    print("Finished sending")

# Function sending UDP packet
def send_second_round() :
    udp_pkt = IP(dst=TARGET_IP)/UDP(sport=HOST_PORT, dport=TARGET_PORT)/"Complete the lyrics : I'm Mr. Bulldops, I'm Mr. Bulldops, Take a sh*t on the floor, Take off your panties and your pants:"
    while True:
        send(udp_pkt, iface=IFACE, verbose=False)
        time.sleep(7)

# Function sending ICMP packet with the flag
def send_flag() :
    flag_sent = 0
    icmp_pkt = IP(src=HOST_IP, dst=TARGET_IP, )/ICMP(type = 8)/"I like what you got, good job : N3v3rRick1ngM0rty"
    while flag_sent < 21:
        send(icmp_pkt, iface=IFACE, verbose=False)
        time.sleep(7)
        flag_sent += 1
    print("Flag has been sent 20 times")

# Function sniff for the first part and waiting for well designed ICMP reply
def received_packet_first() :

    def verif_first_round(pkt : IP) :

        pkt_error = IP(src=HOST_IP, dst=TARGET_IP)/ICMP()
        global first_step_active
        global second_step_active

        # Don't answer packet comming from other sources than the one we send our ICMP packet
        if(pkt[IP].src != TARGET_IP):
            return

        # Don't answer packet if it's not an ICMP packet
        elif(not pkt.haslayer(ICMP)) :
            return 

        else : 
            # Looking that the packet is for us
            if(pkt[IP].dst == HOST_IP) : 
                icmp = pkt.getlayer(ICMP)

                # Looking if the ICMP packet has a payload and if it's not empty
                if(icmp.type == 0 and icmp.payload is not None and len(icmp.payload) > 0) :
                    payload = icmp.payload.load.decode()

                    # Looking that the payload is the right one
                    if(payload == "Show me what you got") :
                        first_step_active = False
                        second_step_active = True
                        pkt_error = pkt_error/"Good job  (Don't answer to this packet)"
                        send(pkt_error, iface=IFACE, verbose=False)
                        return True
                    else :
                        pkt_error = pkt_error/"Almost there, you just sent the wrong payload :/  (Don't answer to this packet)"
                else : 
                    pkt_error = pkt_error/"I think it's not a reply right or you forgot to send something ://  (Don't answer to this packet)"
            else :
                pkt_error = pkt_error/"Wrong destination sorry ://// (Don't answer to this packet)"
            send(pkt_error)
    sniff(stop_filter=verif_first_round, filter="ip", iface=IFACE)


# Function sniff for the second part and waiting for well designed UDP packet
def received_packet_second() :

    def verif_second_round(pkt : IP) :

        pkt_error = IP(src=HOST_IP, dst=TARGET_IP)/ICMP()
        global second_step_active

        # Don't answer packet comming from other sources
        if(pkt[IP].src != TARGET_IP):
            return

        # Don't answer packet if it's not an UDP packet
        elif(not pkt.haslayer(UDP)):
            return

        # Avoiding loop
        elif(pkt.haslayer(ICMP)) :
            return
        else : 

            # Looking that the packet is for us
            if(pkt[IP].dst == HOST_IP) : 
                udp = pkt.getlayer(UDP)
            
                #Looking if the packet is sent to the right port
                if(udp.sport == TARGET_PORT and udp.dport == HOST_PORT) :
            
                    # Looking if the UDP packet has a payload and if it's not empty
                    if(udp.payload is not None and len(udp.payload) > 0) :
                        payload = udp.payload.load.decode()
                        print(payload)
                        print(payload == "It's time to get schwifty in here")

                        # Looking that the payload is the right one
                        if(payload == "It's time to get schwifty in here") :
                            print("all step done")
                            pkt_error = pkt_error/"Good job"
                            second_step_active = False
                            send(pkt_error, iface=IFACE, verbose=False)
                            return True
                        else :
                            pkt_error = pkt_error/"Almost there, you just sent the wrong payload:/  (Don't answer to this packet)"
                    else :
                        pkt_error = pkt_error/"With no payload, you will not go further :// (Don't answer to this packet)"
                else : 
                    pkt_error = pkt_error/"I think you messad with the ports :///  (Don't answer to this packet)"
            else :
                pkt_error = pkt_error/"Wrong source or destination :///// (Don't answer to this packet)"
            # print("I m sending the message did you forget about me ?")
            send(pkt_error, verbose=False)
    sniff(stop_filter=verif_second_round, filter="ip", iface=IFACE)
    print("Finished sniffing")


def main():
    print("Hello world")
    # Preparing the threads
    sending_first_thread = threading.Thread(target=send_first_round)
    receiving_first_thread = threading.Thread(target=received_packet_first)

    sending_second_thread = threading.Thread(target=send_second_round)
    receiving_second_thread = threading.Thread(target=received_packet_second)

    sending_flag_thread = threading.Thread(target=send_flag)

    print("all the thread have been started")
    print("host ip: ", HOST_IP, "host port", HOST_PORT)
    print("target ip:", TARGET_IP, "target port", TARGET_PORT)
    # Starting the first part : Sending an ICMP request and waiting for its ICMP reply 
    sending_first_thread.start()
    receiving_first_thread.start()

    #Waiting the first step has been done before starting the second part
    while first_step_active :
        time.sleep(10)
    print("Finished first step")

    # Starting the second part : Sending an UDP packet and waiting for the right reponse 
    sending_second_thread.start()
    receiving_second_thread.start()

    
    # A debug loop to see if the second part end when they found the flag
    while second_step_active :
        time.sleep(10)
    print("Finished second step")

    # Sending the flag
    sending_flag_thread.start()




if __name__ == "__main__":
    received_ping = False
    main()