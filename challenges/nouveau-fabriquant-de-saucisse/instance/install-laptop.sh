#!/bin/bash

set -e

dhclient
apt-get -y update
apt-get -y install nfs-common gcc cloud-init vim

# user creation
mkdir /home/rougail

