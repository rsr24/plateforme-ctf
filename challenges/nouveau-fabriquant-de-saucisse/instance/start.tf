terraform {
  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
    }
  }
}

provider "libvirt" {
  uri = "qemu:///system"
}
resource "random_string" "flag-admin" {
	length = 15
	special = false
}
resource "random_string" "flag-root" {
	length = 15
	special = false
}

data "template_file" "user_data" {
    template = file("${path.module}/cloud_init.cfg")
    vars = {
      server_ip = "${var.instance_id}"
    }
}

data "template_file" "server_user_data" {
    template = file("${path.module}/cloud_init_server.cfg")
	vars = {
		flag-root = "${random_string.flag-root.result}"
		flag-admin = "${random_string.flag-admin.result}"
	}
}

resource "libvirt_cloudinit_disk" "commoninit" {
    name = "commoninit-${var.instance_id}.iso"
    user_data = data.template_file.user_data.rendered
}

resource "libvirt_cloudinit_disk" "serverinit" {
    name = "serverinit-${var.instance_id}.iso"
    user_data = data.template_file.server_user_data.rendered
}



# Defining VM Volume

resource "libvirt_volume" "ctf-laptop-instance" {
  name = "ctf-laptop-instance-${var.instance_id}.qcow2"
  source = "./ctf-laptop.qcow2"
}
resource "libvirt_volume" "ctf-server-instance" {
  name = "ctf-server-instance-${var.instance_id}.qcow2"
  source = "./ctf-server.qcow2"
}

resource "libvirt_network" "ctf-network" {
    name = "ctf-network-${var.instance_id}"
    mode = "route"
    addresses = ["10.96.${var.instance_id}.0/24"] #TODO: param
}

resource "libvirt_domain" "ctf-laptop-instance" {
  name   = "ctf-laptop-instance-${var.instance_id}"
  memory = "2048"
  vcpu   = 2

  cloudinit = libvirt_cloudinit_disk.commoninit.id

  network_interface {
    network_name = "ctf-network-${var.instance_id}" # List networks with virsh net-list
    addresses = ["10.96.${var.instance_id}.12"] #TODO: param
  }

  disk {
    volume_id = "${libvirt_volume.ctf-laptop-instance.id}"
  }

  console {
    type = "pty"
    target_type = "serial"
    target_port = "0"
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = true
  }
}

resource "libvirt_domain" "ctf-server-instance" {
  name   = "ctf-server-instance-${var.instance_id}"
  memory = "2048"
  vcpu   = 2

  cloudinit = libvirt_cloudinit_disk.serverinit.id

  network_interface {
    network_name = "ctf-network-${var.instance_id}" # List networks with virsh net-list
    addresses = ["10.96.${var.instance_id}.254"] #TODO: param
  }

  disk {
    volume_id = "${libvirt_volume.ctf-server-instance.id}"
  }

  console {
    type = "pty"
    target_type = "serial"
    target_port = "0"
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = true
  }
}

variable "instance_id" {
  type = number
}


# Output Server IP
output "ip" {
  value = "${libvirt_domain.ctf-laptop-instance.network_interface.0.addresses.0}"
}
output "server_ip" {
  value = "${libvirt_domain.ctf-server-instance.network_interface.0.addresses.0}"
}
output "port" {
  value = "22"
}

output "flag-admin" {
	value = "${random_string.flag-admin.result}"
}

output "flag-root" {
	value = "${random_string.flag-root.result}"
}

