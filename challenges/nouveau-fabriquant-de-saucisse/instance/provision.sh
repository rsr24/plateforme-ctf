#!/bin/bash
IMG="$HOME/iso/debian-snapshot.qcow2"

virt-builder debian-12 --format qcow2 --output ctf-server.qcow2 --run install-server.sh --network -v -x
virt-builder debian-12 --format qcow2 --output ctf-laptop.qcow2 --run install-laptop.sh --network -v -x
