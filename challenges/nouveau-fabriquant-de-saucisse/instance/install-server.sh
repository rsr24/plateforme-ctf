#!/bin/bash

set -e

dhclient
apt-get -y update
apt-get -y install nfs-common nfs-kernel-server cloud-init vim
echo > /etc/exports
echo "/srv/home/admin *(rw,sync,no_subtree_check,no_root_squash) 3" >> /etc/exports
echo "/srv/home/rougail *(rw,sync,no_subtree_check,no_root_squash) 3" >> /etc/exports
systemctl enable nfs-kernel-server
mkdir -p /srv/home/admin/.ssh
mkdir -p /srv/home/rougail
chown 1234:1234 -R /srv/home/rougail

# user creation
adduser admin
mkdir -p /home/admin/.ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDZYeHYkehvkGv5dNtSu8yPQLFh+wumseT9FnxpHdbOPECfue3j+cNZ9lHFPV4cDx9WvQ6HaHTtVPrPPP7RL8UKJYCW9S6UiITvOY4PVJpbm1GHEYGqZaB0pT6riT2yTZCij4z68fITZM0xpDg7t9z30nH2E/lc/pm0yOp5ElGFviYpEqusNe88klYpTdt2S1wSV8wNVX/UuH43SCwGMx1loxvzxmIjFNgfqGPKLyhEPUBe0c+JgGKEYwnIeg8uR5IDciLO7w6DQB7N+8Bz0Pioh2rhGhjYClUoryKg+RQsYaxRwzU24e0VgWnTRpEtDJBjxIHC7aa72bpYQ6qyArJFS69IHqIAIKcAnnyMfCCQWs0wx/DpQIV40VkqnuSJKGXGloSp2I1bJxCqYjTA7Vso09hDQLOwrvg3rLl+13psUbf+0VrEBENOkoZLLbLqP1+DLnymBLpxP7MgINh0UgMN+x6kn1fYGxNwC+ALYJHI9jljgIVpoipc1UiW5uZDAQk= root@clea" >> /home/admin/.ssh/authorized_keys
wget https://github.com/carlospolop/PEASS-ng/releases/latest/download/linpeas.sh -O /srv/home/rougail/linpeas.sh
chown 1234:1234 /srv/home/rougail/linpeas.sh
chmod +x /srv/home/rougail/linpeas.sh

