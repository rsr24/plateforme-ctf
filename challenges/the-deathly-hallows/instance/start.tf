terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
    }
  }
}

provider "docker" {
  host = "ssh://${var.dockeruser}@${var.dockerhost}:${var.dockerport}"
}

# Create a container
resource "docker_container" "docker_image_the-deathly-hallows" {
  image = "${var.docker_registry}/the-deathly-hallows:${var.build_version}"
  name  = "docker_${var.instance_id}_the-deathly-hallows"

   upload {
    file    = "/home/ignotus/flag1.txt"
    content = random_string.flag1.result
  }
  upload {
    file    = "/home/cadmus/flag2.txt"
    content = random_string.flag2.result
  }
  upload {
    file    = "/home/antonio/flag3.txt"
    content = random_string.flag3.result
  }
  upload {
    file    = "/root/root.txt"
    content = random_string.root.result
  }

 
  ports {
    internal = 22
   
  }

  ports {
    internal = 80
   
  }

}



variable "instance_id" {
  type = number
}

variable "dockerhost" {
  type = string
}

variable "dockerport" {
  type = string
}

variable "dockeruser" {
  type = string
}

variable "build_version" {
  type = string
}

variable "docker_registry" {
  type = string
}

output "port_22" {
  value = docker_container.docker_image_the-deathly-hallows.ports[0].external
}

output "port_80" {
  value = docker_container.docker_image_the-deathly-hallows.ports[1].external
}


#
# Génération des flag
#
resource "random_string" "flag1" {
  keepers = {instance_id = var.instance_id}
  length = 32
  special = false
  min_upper = 4
  min_lower = 4
  min_numeric = 4
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/flags; echo ${random_string.flag1.result} > ${path.module}/flags/flag1.txt"
  }
}
resource "random_string" "flag2" {
  keepers = {instance_id = var.instance_id}
  length = 32
  special = false
  min_upper = 4
  min_lower = 4
  min_numeric = 4
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/flags; echo ${random_string.flag2.result} > ${path.module}/flags/flag2.txt"
  }
}
resource "random_string" "flag3" {
  keepers = {instance_id = var.instance_id}
  length = 32
  special = false
  min_upper = 4
  min_lower = 4
  min_numeric = 4
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/flags; echo ${random_string.flag3.result} > ${path.module}/flags/flag3.txt"
  }
}
resource "random_string" "root" {
  keepers = {instance_id = var.instance_id}
  length = 32
  special = false
  min_upper = 4
  min_lower = 4
  min_numeric = 4
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/flags; echo ${random_string.root.result} > ${path.module}/flags/root.txt"
  }
}
