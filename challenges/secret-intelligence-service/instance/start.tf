terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
    }
  }
}

provider "docker" {
  host = "ssh://${var.dockeruser}@${var.dockerhost}:${var.dockerport}"
}


resource "docker_container" "docker_kali_image" {
  # using rtmp interception kali image
  image = "${var.docker_registry}/rtmp-interception-kali:${var.build_version}"
  name  = "docker_${var.instance_id}_kali"
  ports {
    internal = 22
  }
  networks_advanced {
    name = docker_network.docker_private_network_main.name
  }
}

resource "docker_container" "docker_image_secret-intelligence-service" {
  # using rtmp interception kali image
  image = "${var.docker_registry}/secret-intelligence-service:${var.build_version}"
  name  = "docker_${var.instance_id}_secret-intelligence-service"
  networks_advanced {
    name = docker_network.docker_private_network_main.name
  }
}

resource "docker_network" "docker_private_network_main" {
  name = "docker_${var.instance_id}_main_subnet"
}

variable "instance_id" {
  type = number
}

variable "dockerhost" {
  type = string
}

variable "dockerport" {
  type = string
}

variable "dockeruser" {
  type = string
}

variable "docker_registry" {
  type = string
}

variable "build_version" {
  type = string
}

output "port" {
  value = docker_container.docker_kali_image.ports[0].external
}

