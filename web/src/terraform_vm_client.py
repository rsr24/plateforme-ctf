import sys
import os
import re
import subprocess
import time
from subprocess import PIPE, check_output, run
from flask import current_app
from .config import Config

config = Config()

VM_HOST_IP = config.VMHOST_HOSTNAME
VM_HOST_USER = config.VMHOST_USER
VM_HOST_PORT = config.VMHOST_PORT


TRIES = 20
DEFAULT_BACKOFF = 1
BACKOFF_MULTIPLIER = 2


def try_ssh_run(command, stdout, stderr, encoding, check):
    """tries to execute a command multiple times until it succeeds or the number of tries is reached"""
    remaining_tries = TRIES
    backoff = DEFAULT_BACKOFF
    current_app.logger.error("==== Trying to execute command ====")
    while True:
        result = None
        try:
            result = subprocess.run(
                command,
                stdout=stdout,
                stderr=stderr,
                encoding=encoding,
                check=check,
            )
            current_app.logger.error(result)
        except subprocess.CalledProcessError as err:
            current_app.logger.error(err)
            if err.returncode != 254 or remaining_tries == 0:  # if number of logins is reached
                raise err
            current_app.logger.error(f"failed with CalledProcessError, sleeping {backoff} seconds")
            time.sleep(backoff)
            backoff *= BACKOFF_MULTIPLIER
            remaining_tries -= 1
            continue
        current_app.logger.error("==== GOT A RESULT ====")
        return result


class TerraformVMClient:
    """Terraform client class that allows to apply, plan and destroy"""

    def __init__(self, instance_id, short_name, variables=None):
        if variables is None:
            variables = {}
        self.terraform_binary = check_output(["which", "terraform"]).decode().strip()
        self.instance_id = instance_id
        self.output = {}
        self.variables = variables
        self.variables["instance_id"] = instance_id
        self.workspace = "workspace{}".format(self.instance_id)
        self.working_directory = "deploy/{}".format(short_name)

    def plan(self):
        pass

    def apply(self):
        """creates the workspace and the VMs associated with it"""

        self.run_command([self.terraform_binary, "init", "-no-color"])
        self.run_command([self.terraform_binary, "workspace", "new", self.workspace])
        command = [self.terraform_binary, "apply", "--auto-approve", "-no-color"]
        result = self.run_command(command, workspace=self.workspace, with_vars=True)
        self.parse_apply_output(result.stdout)

    def parse_apply_output(self, apply_output):
        """parses the output and displays it in a readable way"""

        command = [self.terraform_binary, "output"]
        result = self.run_command(command, workspace=self.workspace)
        found = re.findall(r"([^=\n]+) = \"([^\n]+)\"", result.stdout)
        for key, value in found:
            self.output[key] = value
        return self.output

    def destroy(self):
        """destroys the differents VM and workspaces created"""

        command = [self.terraform_binary, "destroy", "--auto-approve", "-no-color"]
        result = self.run_command(command, workspace=self.workspace, with_vars=True)
        self.remove_temporary_files()

        current_app.logger.error("terraform_vm_client 67")
        # delete workspace
        command = [self.terraform_binary, "workspace", "delete", self.workspace]
        result = self.run_command(command, with_vars=False)

    def remove_temporary_files(self):
        """removes temporary terraform files"""

        for f in ["terraform.tfstate", "terraform.tfstate.backup"]:
            file_path = os.path.join(self.working_directory, f)
            if os.path.exists(file_path):
                os.remove(file_path)

    def format_variables(self, variables):
        """formats command arguments"""

        variable_array = []
        for variable in [
            str(key) + "=" + str(value) for key, value in variables.items()
        ]:
            variable_array.append("-var")
            variable_array.append(variable)
        return variable_array

    def run_command(self, command, workspace="default", with_vars=False):
        """runs the commands via ssh"""

        current_app.logger.error("terraform_vm_client 70")
        if with_vars:
            formatted_variables = self.format_variables(self.variables)
            command.extend(formatted_variables)

        command_list = [f"{self.terraform_binary} workspace select {workspace};"] + command
        encoding = "utf-8"

        current_app.logger.error(f"running command with instance id {self.instance_id}")
        cmd_array = ["ssh", f"{VM_HOST_USER}@{VM_HOST_IP}", f"cd {self.working_directory} && {' '.join(command_list)}"]
        current_app.logger.error(cmd_array)
        result = try_ssh_run(
            cmd_array,
            stdout=PIPE,
            stderr=PIPE,
            encoding=encoding,
            check=True
        )
        return result


class TerraformVMError(Exception):
    pass


if __name__ == "__main__":
    tf = TerraformVMClient(sys.argv[2])
    if sys.argv[1] == 'apply':
        tf.apply()
    elif sys.argv[1] == 'destroy':
        tf.destroy()
