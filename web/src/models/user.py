#  CTF Platform by RSR, educational platform to try cyber-security challenges
#  Copyright (C) 2022 ENSEIRB-MATMECA, Bordeaux-INP, RSR formation since 2018
#  Supervised by Toufik Ahmed, tad@labri.fr
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import random
from datetime import datetime, timedelta

import paramiko
from flask import current_app
from flask_login import UserMixin
from sqlalchemy.orm import relationship
from sqlalchemy.schema import Column
from sqlalchemy.sql import func
from sqlalchemy.types import Boolean, DateTime, Integer, String
from flask_bcrypt import check_password_hash, generate_password_hash

from ..extensions import db, login

import subprocess


def generate_wireguard_keys():
    """
    Generate a WireGuard private & public key
    Requires that the 'wg' command is available on PATH
    Returns (private_key, public_key), both strings
    """
    privkey = subprocess.check_output(
        "wg genkey", shell=True).decode("utf-8").strip()
    pubkey = subprocess.check_output(
        f"echo '{privkey}' | wg pubkey", shell=True).decode("utf-8").strip()
    return (privkey, pubkey)


class User(UserMixin, db.Model):
    id = Column(Integer, primary_key=True)

    # Personal data
    username = Column(String(16), index=True, unique=True, nullable=False)
    email = Column(String(120), index=True, unique=True, nullable=False)
    password = Column(String(128))
    team_name = Column(String(16))

    # Personal config
    account_creation_date = Column(DateTime, default=func.now())
    night_mode = Column(Boolean, default=False)
    is_admin = Column(Boolean, default=False)  # top security
    is_banned = Column(Boolean, default=False)
    is_connected_to_cas = Column(Boolean, default=False)

    # VPN config
    vpn_random_id = Column(String(64))
    vpn_config_file = Column(String(8192))
    vpn_expiration = Column(DateTime)

    instance = relationship(
        "ResourceInstance", uselist=False, back_populates="user")
    validations = relationship("ChallengeValidation", back_populates="user")

    def can_access_logged_content(self):
        return self.is_authenticated and not self.is_banned

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def get_points_for_challenge(self, challenge):
        points = 0
        for validation in self.validations:
            if validation.step.section.challenge == challenge:
                points += validation.step.points
        return points

    # VPN FUNCTIONS

    def is_vpn_configured(self):
        return self.vpn_random_id

    def vpn_remaining_time(self):
        if not self.is_vpn_configured():
            return timedelta(0)
        remaining = self.vpn_expiration - datetime.now()
        return remaining if remaining > timedelta(0) else timedelta(0)

    def get_ssh_client(self):
        ssh_key = paramiko.RSAKey.from_private_key_file("/root/.ssh/id_rsa")
        ssh_client = paramiko.SSHClient()
        ssh_client.load_host_keys("/root/.ssh/known_hosts")
        ssh_client.load_system_host_keys()
        # Should not be necessary, but sometimes the load_... functions are buggy
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh_client.connect(
            hostname=current_app.config["VPN_HOSTNAME"],
            port=current_app.config["VPN_PORT"],
            username=current_app.config["VPN_USER"],
            pkey=ssh_key,
        )
        return ssh_client

    def revoke_current_vpn_configuration(self):
        if not self.is_vpn_configured():
            return

        ssh_client = self.get_ssh_client()

        command = f"wg set wg0 peer \"{self.vpn_random_id}\" remove"
        _, stdout, stderr = ssh_client.exec_command(command)

        # Needed for the revocation to work ?
        stdout.readlines()
        stderr.readlines()

        ssh_client.close()

        self.vpn_random_id = ""
        self.vpn_expiration = datetime.now()

        self.save()

    def generate_new_vpn_configuration(self):
        if self.is_vpn_configured():
            current_app.logger.debug(
                "Revoking current certificate before generating a new one"
            )
            self.revoke_current_vpn_configuration()

        (private_key, public_key) = generate_wireguard_keys()

        ssh_client = self.get_ssh_client()

        quotient_eucl = self.id // 256
        reste_eucl = self.id % 256

        self.vpn_random_id = public_key

        config_expiration = int(current_app.config["VPN_EXPIRATION_TIME_DAYS"])
        new_expiration_days = config_expiration if 0 < config_expiration <= 90 else 30
        self.vpn_expiration = datetime.now() + timedelta(days=new_expiration_days)

        command = f"wg set wg0 peer \"{self.vpn_random_id}\" allowed-ips {current_app.config['VPN_PREFIX']}.{quotient_eucl}.{reste_eucl}"
        _, stdout, stderr = ssh_client.exec_command(command)

        if len(stderr.readlines()) > 0:
            ssh_client.close()
            return False

        ssh_client.close()

        self.save()

        return f"""[Interface]
Address = {current_app.config['VPN_PREFIX']}.{quotient_eucl}.{reste_eucl}
PrivateKey = {private_key}

[Peer]
PublicKey = {current_app.config['VPN_SERVER_PUBLIC_KEY']}
Endpoint = {current_app.config['VPN_SERVER_PUBLIC_IP']}:{current_app.config['VPN_SERVER_PUBLIC_PORT']}
AllowedIPs = {current_app.config['VPN_PREFIX']}.0.0/16,10.2.0.0/16,10.3.255.254,10.96.0.0/16
PersistentKeepalive = 25"""

    def __repr__(self):
        return f"{self.username}"


@login.user_loader
def load_user(user_id):
    return User.get(user_id)
