# This file declares the needed dependencies so we (the CI pipeline) can setup
# the needed terraform cache (so the whole platform run in offline mode)

terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.11.0"
    }
    null = {
      source = "hashicorp/null"
      version = "3.2.1"
    }
    random = {
      source = "hashicorp/random"
      version = "3.4.3"
    }
  }
}
